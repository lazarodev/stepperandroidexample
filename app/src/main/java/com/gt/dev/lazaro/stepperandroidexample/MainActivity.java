package com.gt.dev.lazaro.stepperandroidexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.gt.dev.lazaro.stepperandroidexample.fragments.MyStepperAdapter;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

public class MainActivity extends AppCompatActivity {

    private StepperLayout stepperLayout;
    private MyStepperAdapter stepperAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Remember this StepperLayout it is on our activity_main.xml
        stepperLayout = (StepperLayout) findViewById(R.id.stepper_layout);
        // Instance for our adapter
        stepperAdapter = new MyStepperAdapter(getSupportFragmentManager(), this);
        // Set adapter to StepperLayout
        stepperLayout.setAdapter(stepperAdapter);
    }

}
