package com.gt.dev.lazaro.stepperandroidexample.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by lazarodev on 10/7/2017.
 */

public class MyStepperAdapter extends AbstractFragmentStepAdapter {

    private static final String CURRENT_STEP_POSITION_KEY = "messageResourceId";

    /**
     * This constructor it is for use it in the MainActivity
     *
     * @param fm
     * @param context
     */
    public MyStepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    /**
     * With CreateStep method we show our Fragments Step
     *
     * @param position
     * @return
     */
    @Override
    public Step createStep(int position) {
        switch (position) {
            case 0:
                final StepFragmentSample step1 = new StepFragmentSample();
                Bundle b = new Bundle();
                b.putInt(CURRENT_STEP_POSITION_KEY, position);
                step1.setArguments(b);
                return step1;
            case 1:
                final Step2 step2 = new Step2();
                Bundle b2 = new Bundle();
                b2.putInt(CURRENT_STEP_POSITION_KEY, position);
                step2.setArguments(b2);
                return step2;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    /**
     * In this method we'll set the title for the respective
     * view or fragment step
     *
     * @param position
     * @return
     */
    @NonNull
    @Override
    public StepViewModel getViewModel(int position) {
        switch (position) {
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Guasap")
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Guasap2")
                        .create();
        }
        return null;
    }
}
